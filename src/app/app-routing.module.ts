import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AboutComponent } from './about/about.component';
import { UserDetailComponent } from './user/user-detail/user-detail.component';
import { UserFormComponent } from './user/user-form/user-form.component';
import { UserComponent } from './user/user.component';

const routes: Routes = [
  {path: '', pathMatch: 'full', redirectTo: 'Users'},
  {path:'Users', component: UserComponent, children: [
    {path: 'New', pathMatch: 'full', component: UserFormComponent},
    {path: ':id', pathMatch: 'full', component: UserDetailComponent},
  ]},
  {path: 'About', pathMatch: 'full', component: AboutComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
