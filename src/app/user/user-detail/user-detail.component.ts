import { Component, OnInit } from '@angular/core';
import { DataSharingService } from '../DataSharingService';
import { User } from '../User';
import { IUserRepo } from '../IUserRepo';

declare function displaybuttons(): void;
@Component({
  selector: 'app-user-detail',
  templateUrl: './user-detail.component.html',
  styleUrls: ['./user-detail.component.css']
})
export class UserDetailComponent implements OnInit {
  user: User|undefined;
  constructor(private dataSharingService: DataSharingService, private userRepo: IUserRepo) { }

  ngOnInit(): void {
    this.dataSharingService.currentUser.subscribe(user => this.user = user)
    
  }

  deleteUser(user: User|undefined){
    this.userRepo.deleteUser(user);
  }
  displayButtons(){
    displaybuttons();
  }

}
