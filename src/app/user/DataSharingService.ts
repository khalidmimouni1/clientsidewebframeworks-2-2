import { Injectable } from "@angular/core";
import { BehaviorSubject, Observable } from "rxjs";
import { User } from "./User";
import { IUserRepo } from "./IUserRepo";
@Injectable()
export class DataSharingService{
    public dataSource:BehaviorSubject<User|undefined>;
    public currentUser: Observable<User|undefined>;
    constructor(private userRepo:IUserRepo){
        this.dataSource = new BehaviorSubject<User|undefined>(userRepo.getUsers()[0]);
        this.currentUser = this.dataSource.asObservable();
        console.log("DataSharingService geinstantieerd")
    }
    changeCurrentUser(newUser: User){
        this.dataSource.next(newUser)
    }
}