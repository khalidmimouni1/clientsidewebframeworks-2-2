

import { User } from "./User";

export abstract class IUserRepo{
    public abstract getUsers(): User[]
    public abstract getUserById(id: number): User | undefined
    public abstract addUser(user: User): void
    public abstract deleteUser(user: User|undefined): void
}