import { Injectable } from "@angular/core";
import { IUserRepo } from "./IUserRepo";
import { User } from "./User";
@Injectable()
export class UserRepo implements IUserRepo{
    userList:User[];

    constructor(){
        this.userList = [];
        var Khalid = new User(1, 'Khalid', 'Mimouni', 639308067, 'khalid_mi@hotmail.com');
        var Hans = new User(2, 'Hans', 'Klok', 639308067, 'hansklok@hotmail.com');
        this.userList.push(Khalid, Hans);
    }
    getUsers(): User[] {
        return this.userList;
    }
    getUserById(id: number): User | undefined{
        return this.userList.find(u => u.id == id);
    }

    addUser(user: User){
        this.userList.push(user);
        console.log("user dat toegevoegd wordt in de repo: " + JSON.stringify(user))
        console.log("de lijst van users in de repo is nu: " + JSON.stringify(this.userList));
    }

    deleteUser(user: User|undefined){
        if(user instanceof User){
            this.userList.splice(this.userList.indexOf(user),1);
        }
        
    }

}