

export class User{
    id?: number;
    firstName?: string;
    lastName?: string;
    phoneNumber?: number;
    email?: string;

    constructor(id?: number, firstName?:string, lastName?:string, phoneNumber?:number , email?:string){
        this.id = id;
        this.firstName= firstName;
        this.lastName = lastName;
        this.phoneNumber = phoneNumber;
        this.email = email;
    }
    
}